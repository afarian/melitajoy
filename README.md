# melitajoy

Website for author [Melita Joy](https://www.melitajoy.com/) running on [Jekyll](https://jekyllrb.com/).

### Instructions
```
gem install bundler jekyll
bundler exec jekyll serve
```
